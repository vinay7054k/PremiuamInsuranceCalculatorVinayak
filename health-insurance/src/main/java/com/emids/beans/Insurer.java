package com.emids.beans;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Insurer {
	private String name;
	
	private String gender;
	
	private int age;
	
	/*Current health*/
	private String Hypertension;
	
	private String bloodPressure;
	
	private String bloodSugar;
	
	private String overweight;
	
	/*Habits*/
	
	private List<String> goodHabits;
	private List<String> badHabits;
	/*private boolean smoking;
	
	private boolean alcohol;
	
	private boolean dailyExercise;
	
	private boolean drugs;
*/		
}
