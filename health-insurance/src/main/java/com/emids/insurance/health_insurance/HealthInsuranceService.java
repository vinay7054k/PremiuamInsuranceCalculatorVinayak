package com.emids.insurance.health_insurance;

import java.util.ArrayList;
import java.util.List;

import com.emids.beans.Insurer;
import com.emids.insurance.constants.Constants;

/**
 * Hello world!
 *
 */

public class HealthInsuranceService 
{
	public static void main( String[] args )
    {
        Insurer insurer = new Insurer();
        insurer.setName("Norman Gomes");
        insurer.setAge(34);
        insurer.setGender(Constants.MALE);
        insurer.setBloodPressure(Constants.NO);
        insurer.setBloodSugar(Constants.NO);
        insurer.setHypertension(Constants.NO);
        insurer.setOverweight(Constants.YES);
        
        List<String> goodHabits=new ArrayList<String>();
        goodHabits.add("dailyExcercise");
        insurer.setGoodHabits(goodHabits);
        List<String> badHabits=new ArrayList<String>();
        //badHabits.add("drugs");
        badHabits.add("alcohol");
        //badHabits.add("smoking");
        insurer.setBadHabits(badHabits);
        
        PrimiumCaluculatorService service =new PrimiumCaluculatorService();
        double calculatedPremium = Constants.basePremium;
        calculatedPremium = service.getIncreaseOnAge(insurer.getAge(),calculatedPremium);
        calculatedPremium = service.getIncreaseOnGender(insurer.getGender(),calculatedPremium);
        calculatedPremium = service.getIncreaseOnCurrentHealthStatus(insurer,calculatedPremium);
        calculatedPremium = service.getIncreaseOnHabits(insurer,calculatedPremium);
        
        System.out.println("Health Insurance Premium for "+ insurer.getName()+": " + calculatedPremium);
        
    }
}
