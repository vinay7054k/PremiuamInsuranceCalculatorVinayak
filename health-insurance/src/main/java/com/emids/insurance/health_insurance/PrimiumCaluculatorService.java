package com.emids.insurance.health_insurance;

import com.emids.beans.Insurer;
import com.emids.insurance.constants.Constants;

public class PrimiumCaluculatorService {
	
	public double getIncreaseOnAge(int age,double amount){
		
		if( age<=0 ) return 0.0;// OR we can throw Exception
		
		else if(age<18){
			return Constants.basePremium;
		}
		else if(age>=18 && age<40){
			return getIncrementedAmount(age,0.1,amount);
		}else{
			return getIncrementedAmount(age,0.2,amount);
		}
	}

	private double getIncrementedAmount(int age,double per,double amount){
		int inc= (age-18)/5;			
		for(int i=0;i<inc;i++){
			amount+=amount*per;
		}
		return amount;
	}

	public double getIncreaseOnCurrentHealthStatus(Insurer insurer,double amount) {
		if(insurer.getHypertension().equals(Constants.YES)){
			amount+=amount*0.01;
		}
		if(insurer.getBloodPressure().equals(Constants.YES)){
			amount+=amount*0.01;
		}
		if(insurer.getBloodSugar().equals(Constants.YES)){
			amount+=amount*0.01;
		}
		if(insurer.getOverweight().equals(Constants.YES)){
			amount+=amount*0.01;
		}
		return amount;
	}

	public double getIncreaseOnGender(String gender, double amount) {
		if(gender.equalsIgnoreCase(Constants.MALE)){
			return amount+(amount*0.02);
		}
		return amount;
	}

	public double getIncreaseOnHabits(Insurer insurer, double amount) {
		for(int i=0;i<insurer.getGoodHabits().size();i++){
			amount-=amount*0.03;
		}
		for(int i=0;i<insurer.getBadHabits().size();i++){
			amount+=amount*0.03;
		}
		return amount;
	}
}
